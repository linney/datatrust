#!/bin/bash
set -m
yarn run hardhat node --hostname 0.0.0.0 &
yarn run hardhat run --network localhost scripts/deploy.ts
fg
