import Web3 from "web3";
import Prompt from "prompt";

async function main() {
  Prompt.start();
  Prompt.get(["nonce"], async function (_: any, result: { nonce: string }) {
    const nonce = result.nonce;
    const web3 = new Web3(
      new Web3.providers.HttpProvider("http://localhost:8545")
    );
    const account = (await web3.eth.getAccounts())[0];
    console.log(account);
    const signature = await web3.eth.sign(nonce, account);
    console.log(`Signature: ${signature}`);
  });
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
