import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { expect, assert } from "chai";
// @ts-ignore - Because we have a `hardhat.config.ts` file, it thinks that's where we're importing from. Its not.
import { ethers } from "hardhat";
import { Datatrust } from "../typechain";

describe("Datatrust", function () {
  it("Should allow creation of a datatrust", async function () {
    const DatatrustManager = await ethers.getContractFactory(
      "DatatrustManager"
    );
    const datatrustManager = await DatatrustManager.deploy();
    await (
      await datatrustManager.addDatatrust(
        "Example Trust",
        "Example",
        "University of Manitoba"
      )
    ).wait();
  });

  describe("Elections", function () {
    let datatrust: Datatrust;
    let users: SignerWithAddress[];

    async function setup_n_user_trust(n: number) {
      const stakeholders = [users[0]];

      for (let i = 1; i < n; i++) {
        const next_canditate = users[i];
        // Propose a new stakeholder
        const candidate_proposal_tx = await datatrust.proposeCandidate(
          next_canditate.address
        );
        await candidate_proposal_tx.wait();

        // Vote for the new stakeholder with each current stakeholder
        for (const stakeholder of stakeholders) {
          const vote_tx = await datatrust
            .connect(stakeholder)
            .vote(next_canditate.address, true);
          await vote_tx.wait();
        }

        // Close the election manually. This is normall done via polling
        const poll_tx = await datatrust.closeElection(next_canditate.address);
        await poll_tx.wait();

        stakeholders.push(next_canditate);
      }
    }

    before(async function () {
      users = await ethers.getSigners();
    });

    beforeEach("Create a Datatrust", async function () {
      const DatatrustManager = await ethers.getContractFactory(
        "DatatrustManager"
      );
      const datatrustManager = await DatatrustManager.deploy();
      await (
        await datatrustManager.addDatatrust(
          "Example Trust",
          "Example",
          "University of Manitoba"
        )
      ).wait();

      const datatrust_addrs = await datatrustManager.getDatatrusts();
      datatrust = await ethers.getContractAt("Datatrust", datatrust_addrs[0]);
    });

    describe("Proposals", function () {
      it("Should allow stakeholders to be added", async function () {
        const alternative_user = users[1];

        // Propose a new stakeholder
        const candidate_proposal_tx = await datatrust.proposeCandidate(
          alternative_user.address
        );
        await candidate_proposal_tx.wait();

        // Vote that stakeholder in
        const vote_tx = await datatrust.vote(alternative_user.address, true);
        await vote_tx.wait();

        // Close the election manually. This is normall done via polling
        const poll_tx = await datatrust.closeElection(alternative_user.address);
        await poll_tx.wait();

        // Get the new stakeholders
        const stakeholders = await datatrust.getStakeholders();
        assert.equal(stakeholders.length, 2);
        assert.isTrue(stakeholders.includes(alternative_user.address));
      });

      it("Should not allow non-stakeholders to propose candidates", async function () {
        const alternative_user = users[1];

        // Propose a new stakeholder with an invalid address, which should fail
        expect(
          datatrust
            .connect(alternative_user)
            .proposeCandidate(alternative_user.address)
        ).to.be.revertedWith("Only stakeholder can propose a candidate");
      });

      it("Should not allow votes on a closed election", async function () {
        const [stakeholder_a, stakeholder_b, other_user] = users;
        // Propose a new stakeholder
        const candidate_proposal_tx = await datatrust.proposeCandidate(
          stakeholder_b.address
        );
        await candidate_proposal_tx.wait();

        // Vote that stakeholder in
        const vote_tx = await datatrust
          .connect(stakeholder_a)
          .vote(stakeholder_b.address, true);
        await vote_tx.wait();

        // Close the election manually. This is normall done via polling
        const poll_tx = await datatrust.closeElection(stakeholder_b.address);
        await poll_tx.wait();

        // Propose another new stakeholder
        const candidate_proposal_tx2 = await datatrust.proposeCandidate(
          other_user.address
        );
        await candidate_proposal_tx2.wait();

        // Vote against that stakeholder
        const vote_tx2 = await datatrust.vote(other_user.address, false);
        await vote_tx2.wait();

        // Try to vote with the other stakeholder
        expect(
          datatrust.connect(stakeholder_b).vote(other_user.address, true)
        ).to.be.revertedWith("Candidate not proposed");
      });

      it("Should not allow stakeholders to be proposed as candidates", async function () {
        const stakeholder = users[0];
        expect(
          datatrust.connect(stakeholder).proposeCandidate(stakeholder.address)
        ).to.be.revertedWith("Cannot propose a current stakeholder");
      });

      it("Should not allow stakeholders to proposed multiple times at once", async function () {
        const stakeholder = users[1];

        const candidate_proposal_tx = await datatrust.proposeCandidate(
          stakeholder.address
        );
        await candidate_proposal_tx.wait();

        expect(
          datatrust.proposeCandidate(stakeholder.address)
        ).to.be.revertedWith("Candidate already proposed");
      });

      it("Should not allow pre-emptive closing of elections", async function () {
        const alternative_user = users[1];

        // Propose a new stakeholder
        const candidate_proposal_tx = await datatrust.proposeCandidate(
          alternative_user.address
        );
        await candidate_proposal_tx.wait();

        // Try to close the election
        expect(
          datatrust
            .connect(alternative_user)
            .closeElection(alternative_user.address)
        ).to.be.revertedWith("Election not closed");
      });

      it("Should allow large amounts of stakeholders {n=10}", async function () {
        await setup_n_user_trust(10);
      });
    });

    describe("Removals", function () {
      it("Should allow removing a stakeholder", async function () {
        const [a, b] = users;
        await setup_n_user_trust(2);

        const rm_tx = await datatrust.proposeRemoval(b.address);
        await rm_tx.wait();

        const rm_vote_tx = await datatrust.voteRemove(b.address, true);
        await rm_vote_tx.wait();

        const rm_poll_tx = await datatrust.closeRemoval(b.address);
        await rm_poll_tx.wait();

        const stakeholders = await datatrust.getStakeholders();

        expect(stakeholders.length).to.equal(1);
        expect(stakeholders[0]).to.equal(a.address);
      });

      it("Should not remove users if a stakeholder disagrees", async function () {
        const [a, b] = users;
        await setup_n_user_trust(2);

        const rm_tx = await datatrust.proposeRemoval(b.address);
        await rm_tx.wait();

        const rm_vote_tx = await datatrust.voteRemove(b.address, false);
        await rm_vote_tx.wait();

        expect(datatrust.closeRemoval(b.address)).to.be.revertedWith(
          "Candidate not proposed"
        );

        const stakeholders = await datatrust.getStakeholders();
        expect(stakeholders.length).to.equal(2);
        expect(stakeholders).to.include(a.address);
        expect(stakeholders).to.include(b.address);
      });

      it("Should not allow stakeholders to vote in their own removals", async function () {
        const [a, b] = users;
        await setup_n_user_trust(2);

        const rm_tx = await datatrust.proposeRemoval(b.address);
        await rm_tx.wait();

        expect(
          datatrust.connect(b).voteRemove(b.address, false)
        ).to.be.revertedWith("Cannot vote for self");
      });
    });
  });

  describe("Users", function () {
    let datatrust: Datatrust;
    let users: SignerWithAddress[];

    async function setup_n_user_trust(n: number) {
      const stakeholders = [users[0]];

      for (let i = 1; i < n; i++) {
        const next_canditate = users[i];
        // Propose a new stakeholder
        const candidate_proposal_tx = await datatrust.proposeCandidate(
          next_canditate.address
        );
        await candidate_proposal_tx.wait();

        // Vote for the new stakeholder with each current stakeholder
        for (const stakeholder of stakeholders) {
          const vote_tx = await datatrust
            .connect(stakeholder)
            .vote(next_canditate.address, true);
          await vote_tx.wait();
        }

        // Close the election manually. This is normall done via polling
        const poll_tx = await datatrust.closeElection(next_canditate.address);
        await poll_tx.wait();

        stakeholders.push(next_canditate);
      }
    }

    before(async function () {
      users = await ethers.getSigners();
    });

    beforeEach("Create a Datatrust", async function () {
      const DatatrustManager = await ethers.getContractFactory(
        "DatatrustManager"
      );
      const datatrustManager = await DatatrustManager.deploy();
      await (
        await datatrustManager.addDatatrust(
          "Example Trust",
          "Example",
          "University of Manitoba"
        )
      ).wait();

      const datatrust_addrs = await datatrustManager.getDatatrusts();
      datatrust = await ethers.getContractAt("Datatrust", datatrust_addrs[0]);
    });

    it("Should allow users to be voted into the Datatrust", async function () {
      const user = users[4];

      const user_proposal_tx = await datatrust.proposeUser(user.address);
      await user_proposal_tx.wait();

      const user_vote_tx = await datatrust.voteUser(user.address, true);
      await user_vote_tx.wait();

      const user_poll_tx = await datatrust.closeUserElection(user.address);
      await user_poll_tx.wait();

      const d_users = await datatrust.getUsers();

      expect(d_users.length).to.equal(1);
      expect(d_users[0]).to.equal(user.address);
    });

    it("Should require consensus when voting in users", async function () {
      const user = users[4];
      const stakeholders = [users[1], users[2]];

      await setup_n_user_trust(3);

      const user_proposal_tx = await datatrust.proposeUser(user.address);
      await user_proposal_tx.wait();

      for (const stakeholder of stakeholders) {
        const user_vote_tx = await datatrust
          .connect(stakeholder)
          .voteUser(user.address, true);
        await user_vote_tx.wait();
      }

      const user_vote_tx = await datatrust.voteUser(user.address, false);
      await user_vote_tx.wait();

      // This is expected as the election should close on the no vote.
      expect(datatrust.closeUserElection(user.address)).to.be.revertedWith(
        "User not proposed"
      );

      const d_users = await datatrust.getUsers();

      expect(d_users.length).to.equal(0);
    });

    it("Should allow any stakeholder to remove a user", async function () {
      const user = users[4];

      const user_proposal_tx = await datatrust.proposeUser(user.address);
      await user_proposal_tx.wait();

      const user_vote_tx = await datatrust.voteUser(user.address, true);
      await user_vote_tx.wait();

      const user_poll_tx = await datatrust.closeUserElection(user.address);
      await user_poll_tx.wait();

      let d_users = await datatrust.getUsers();

      expect(d_users.length).to.equal(1);
      expect(d_users[0]).to.equal(user.address);

      const user_remove_vote_tx = await datatrust.removeUser(user.address);
      await user_remove_vote_tx.wait();

      d_users = await datatrust.getUsers();
      expect(d_users.length).to.equal(0);
    });
  });
});
