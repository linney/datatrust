//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

contract Datatrust {
  struct StakeholderVote {
    bool vote;
    bool hasVoted;
  }

  struct Election {
    mapping(address => StakeholderVote) votes;
    address candidate;
    uint256 start;
    bool nayVote;
    bool active;
  }

  string public name;
  string public description;
  string public source;
  address[] public stakeHolders;
  address[] public users;

  mapping(address => Election) public userElections;
  address[] public candidateUsers;

  mapping(address => Election) public elections;
  address[] public candidates;

  mapping(address => Election) public removals;
  address[] public removalsCandidates;

  constructor(
    string memory _name,
    string memory _description,
    string memory _source,
    address _owner
  ) {
    name = _name;
    description = _description;
    source = _source;
    stakeHolders = new address[](0);
    stakeHolders.push(_owner);
  }

  function getUsers() public view returns (address[] memory) {
    return users;
  }

  function getCandidateUsers() public view returns (address[] memory) {
    return candidateUsers;
  }

  function CheckIfUserIsUser(address _user) public view returns (bool) {
    for (uint256 i = 0; i < users.length; i++) {
      if (users[i] == _user) {
        return true;
      }
    }
    return false;
  }

  function proposeUser(address _user) public {
    require(
      checkIfUserIsStakeholder(msg.sender),
      "Only stakeholder can propose a user"
    );

    require(!CheckIfUserIsUser(_user), "Cannot propose a current user");

    require(!userElections[_user].active, "Candidate already proposed");

    Election storage election = userElections[_user];
    election.candidate = _user;
    election.start = block.timestamp;
    election.nayVote = false;
    election.active = true;

    candidateUsers.push(_user);
  }

  function voteUser(address candidate, bool _vote) public {
    require(checkIfUserIsStakeholder(msg.sender), "Only stakeholder can vote");
    require(userElections[candidate].active, "Candidate not proposed");

    // Log Vote
    userElections[candidate].votes[msg.sender].vote = _vote;
    userElections[candidate].votes[msg.sender].hasVoted = true;

    // If they voted nay, close the election
    if (!_vote) {
      userElections[candidate].nayVote = true;
      closeUserElection(candidate);
    }
  }

  function removeUser(address _user) public {
    require(
      checkIfUserIsStakeholder(msg.sender),
      "Only stakeholder can remove a user"
    );
    require(CheckIfUserIsUser(_user), "Cannot remove a non-user");

    for (uint256 i = 0; i < users.length; i++) {
      if (users[i] == _user) {
        users[i] = users[users.length - 1];
        users.pop();
        break;
      }
    }
  }

  function getRemovalCandidates() public view returns (address[] memory) {
    return removalsCandidates;
  }

  function getCandidates() public view returns (address[] memory) {
    return candidates;
  }

  function getStakeholders() public view returns (address[] memory) {
    return stakeHolders;
  }

  function proposeCandidate(address candidate) public {
    require(
      checkIfUserIsStakeholder(msg.sender),
      "Only stakeholder can propose a candidate"
    );

    require(
      !checkIfUserIsStakeholder(candidate),
      "Cannot propose a current stakeholder"
    );

    require(!elections[candidate].active, "Candidate already proposed");

    Election storage election = elections[candidate];
    election.candidate = candidate;
    election.start = block.timestamp;
    election.nayVote = false;
    election.active = true;

    candidates.push(candidate);
  }

  function proposeRemoval(address candidate) public {
    require(
      checkIfUserIsStakeholder(msg.sender),
      "Only stakeholder can propose a removal candidate"
    );

    require(
      checkIfUserIsStakeholder(candidate),
      "Cannot propose a non-stakeholder for removal"
    );

    require(!removals[candidate].active, "Candidate already proposed");

    Election storage election = removals[candidate];
    election.candidate = candidate;
    election.start = block.timestamp;
    election.nayVote = false;
    election.active = true;

    removalsCandidates.push(candidate);
  }

  function vote(address candidate, bool _vote) public {
    require(checkIfUserIsStakeholder(msg.sender), "Only stakeholder can vote");
    require(elections[candidate].active, "Candidate not proposed");

    // Log Vote
    elections[candidate].votes[msg.sender].vote = _vote;
    elections[candidate].votes[msg.sender].hasVoted = true;

    // If they voted nay, close the election
    if (!_vote) {
      elections[candidate].nayVote = true;
      closeElection(candidate);
    }
  }

  function voteRemove(address candidate, bool _vote) public {
    require(checkIfUserIsStakeholder(msg.sender), "Only stakeholder can vote");
    require(candidate != msg.sender, "Cannot vote for self");
    require(removals[candidate].active, "Candidate not proposed");

    // Log Vote
    removals[candidate].votes[msg.sender].vote = _vote;
    removals[candidate].votes[msg.sender].hasVoted = true;

    // If they voted nay, close the election
    if (!_vote) {
      removals[candidate].nayVote = true;
      closeRemoval(candidate);
    }
  }

  function closeRemoval(address candidate) public {
    require(removals[candidate].active, "Candidate not proposed");
    require(
      removals[candidate].start + 1 days <= block.timestamp ||
        removals[candidate].nayVote ||
        checkIfAllVotedRemoval(candidate),
      "Election not closed"
    );

    // Remove
    if (!removals[candidate].nayVote) {
      for (uint256 i = 0; i < stakeHolders.length; i++) {
        address stakeholder = stakeHolders[i];

        if (stakeholder == candidate) {
          // Remove from stakeholders, we do it this way instead of delete()
          // because it preserves the array size, and the order is not important
          stakeHolders[i] = stakeHolders[stakeHolders.length - 1];
          stakeHolders.pop();
          break;
        }
      }
    }

    // Cleanup
    delete removals[candidate];
    for (uint256 i = 0; i < removalsCandidates.length; i++) {
      if (removalsCandidates[i] == candidate) {
        // Remove from candidates, we do it this way instead of delete()
        // because it preserves the array size, and the order is not important
        removalsCandidates[i] = removalsCandidates[
          removalsCandidates.length - 1
        ];
        removalsCandidates.pop();
        break;
      }
    }
  }

  function checkIfAllVotedRemoval(address candidate)
    public
    view
    returns (bool)
  {
    Election storage election = removals[candidate];
    require(election.active, "Candidate not proposed");

    for (uint256 j = 0; j < stakeHolders.length; j++) {
      address stakeholder = stakeHolders[j];

      if (stakeholder == candidate) {
        continue;
      }

      if (!election.votes[stakeholder].hasVoted) {
        return false;
      }
    }

    return true;
  }

  function checkIfAllVotedElection(address candidate)
    public
    view
    returns (bool)
  {
    Election storage election = elections[candidate];
    require(election.active, "Candidate not proposed");

    for (uint256 j = 0; j < stakeHolders.length; j++) {
      address stakeholder = stakeHolders[j];

      if (!election.votes[stakeholder].hasVoted) {
        return false;
      }
    }

    return true;
  }

  function checkIfAllVotedUserElection(address candidate)
    public
    view
    returns (bool)
  {
    Election storage election = userElections[candidate];
    require(election.active, "Candidate not proposed");

    for (uint256 j = 0; j < stakeHolders.length; j++) {
      address stakeholder = stakeHolders[j];

      if (!election.votes[stakeholder].hasVoted) {
        return false;
      }
    }

    return true;
  }

  function closeElection(address candidate) public {
    require(elections[candidate].active, "Candidate not proposed");
    require(
      elections[candidate].start + 1 days <= block.timestamp ||
        elections[candidate].nayVote ||
        checkIfAllVotedElection(candidate),
      "Election not closed"
    );

    // Elect
    if (!elections[candidate].nayVote) {
      stakeHolders.push(candidate);
    }

    // Cleanup
    delete elections[candidate];
    for (uint256 i = 0; i < candidates.length; i++) {
      if (candidates[i] == candidate) {
        // Remove from candidates, we do it this way instead of delete()
        // because it preserves the array size, and the order is not important
        candidates[i] = candidates[candidates.length - 1];
        candidates.pop();
        break;
      }
    }
  }

  function closeUserElection(address candidate) public {
    require(userElections[candidate].active, "User not proposed");
    require(
      userElections[candidate].start + 1 days <= block.timestamp ||
        userElections[candidate].nayVote ||
        checkIfAllVotedUserElection(candidate),
      "Election not closed"
    );

    // Elect
    if (!userElections[candidate].nayVote) {
      users.push(candidate);
    }

    // Cleanup
    delete userElections[candidate];
    for (uint256 i = 0; i < candidateUsers.length; i++) {
      if (candidateUsers[i] == candidate) {
        // Remove from candidates, we do it this way instead of delete()
        // because it preserves the array size, and the order is not important
        candidateUsers[i] = candidateUsers[candidateUsers.length - 1];
        candidateUsers.pop();
        break;
      }
    }
  }

  function checkIfUserIsStakeholder(address user) public view returns (bool) {
    for (uint256 i = 0; i < stakeHolders.length; i++) {
      if (stakeHolders[i] == user) {
        return true;
      }
    }
    return false;
  }
}

contract DatatrustManager {
  Datatrust[] public datatrusts;

  constructor() {
    datatrusts = new Datatrust[](0);
  }

  function getDatatrusts() public view returns (Datatrust[] memory) {
    return datatrusts;
  }

  function addDatatrust(
    string memory _name,
    string memory _description,
    string memory _source
  ) public {
    Datatrust datatrust = new Datatrust(
      _name,
      _description,
      _source,
      msg.sender
    );
    datatrusts.push(datatrust);
  }
}
