module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      height: {
        "fit-content": "fit-content",
      },
    },
  },
  variants: {
    extend: {
      dropShadow: ["hover"],
      ringWidth: ["hover"],
    },
  },
  plugins: [require("@tailwindcss/forms")],
};
