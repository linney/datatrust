export default function Footer() {
    return (
        <>
            <div className="bottom-0 text-center inset-x-0 p-4 fixed">
                <p>
                    This project is licensed under the MIT License. The source
                    code is available at&nbsp;
                    <a
                        href="https://gitlab.com/linney/datatrust"
                        className="text-blue-500 hover:underline"
                    >
                        https://gitlab.com/linney/datatrust
                    </a>
                    .
                </p>
            </div>
        </>
    );
}
