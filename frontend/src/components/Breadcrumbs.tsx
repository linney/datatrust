import { Datatrust } from "../openapi";
import { ChevronRightIcon, DocumentIcon } from "@heroicons/react/solid";
import { NavLink } from "react-router-dom";

export function DatatrustBreadcrumbs(props: {
    datatrust: Datatrust;
    page: {
        name: string;
        path: string;
    };
}) {
    const { name, path } = props.page;
    const { datatrust } = props;

    return (
        <nav className="flex py-4" aria-label="Breadcrumb">
            <ol className="flex items-center space-x-4">
                <li>
                    <div className="flex items-center">
                        <DocumentIcon
                            className="flex-shrink-0 h-5 w-5 text-gray-400"
                            aria-hidden="true"
                        />
                        <NavLink
                            to={`/trust/${datatrust.id}`}
                            className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"
                        >
                            {datatrust.name}
                        </NavLink>
                    </div>
                </li>
                <li>
                    <div className="flex items-center">
                        <ChevronRightIcon
                            className="flex-shrink-0 h-5 w-5 text-gray-400"
                            aria-hidden="true"
                        />
                        <NavLink
                            to={path}
                            className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"
                        >
                            {name}
                        </NavLink>
                    </div>
                </li>
            </ol>
        </nav>
    );
}
