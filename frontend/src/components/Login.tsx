import { useContext, useState } from "react";
import Web3 from "web3";
import { User, UserContext } from "../utils/User";
import { Web3Context } from "../utils/Web3";
import LoginFailureNotification from "./LoginFailureNotification";

function DisplayAccount(props: { user: User }) {
    const { user } = props;

    return (
        <p className="text-sm ml-auto self-center mr-4">
            Active User
            <p className="text-gray-700 font-mono">{user.address}</p>
        </p>
    );
}

export default function Login(props: { className?: string }) {
    const { setWeb3 } = useContext(Web3Context);
    const { user, setUser } = useContext(UserContext);
    const [loginFailure, setLoginFailure] = useState(false);

    function try_metamask() {
        if (!window.hasOwnProperty("ethereum")) {
            setLoginFailure(true);
            return;
        }

        // @ts-ignore - We already verified that the window has the ethereum property
        window.ethereum.send("eth_requestAccounts").then(() => {
            // @ts-ignore - We already verified that the window has the ethereum property
            const web3 = new Web3(window.ethereum);

            setWeb3(web3);
            console.log("metamask connected");
            web3.eth.getAccounts().then((accounts) => {
                setUser({ address: accounts[0] });
                console.log("metamask account", accounts[0]);
            });
        });
    }

    return (
        <>
            {loginFailure ? <LoginFailureNotification /> : null}
            {user !== null ? (
                <DisplayAccount user={user} />
            ) : (
                <button
                    onClick={try_metamask}
                    className={
                        "inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 " +
                        props.className
                    }
                >
                    Login
                </button>
            )}
        </>
    );
}
