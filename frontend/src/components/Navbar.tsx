import { NavLink } from "react-router-dom";
import Login from "./Login";

function PageLink(props: { to: string; name: string }) {
    return (
        <NavLink
            to={props.to}
            className={({ isActive }) =>
                "px-4 py-4 mx-2 font-medium text-base" +
                (isActive
                    ? " border-indigo-500 text-gray-900 border-b-4"
                    : " text-gray-500 hover:border-gray-500 hover:text-gray-900")
            }
        >
            {props.name}
        </NavLink>
    );
}

export default function Navbar() {
    return (
        <nav className="shadow border-black mb-2 flex md:pl-44 md:pr-44 bg-white">
            <PageLink to="/" name="Datatrusts" />
            <PageLink to="/requests" name="Datatrust Requests" />
            <PageLink to="/user" name="User+" />
            <PageLink to="/trust/create" name="Datatrust+" />
            <PageLink to="/trust/1" name="Trust" />
            <PageLink to="/trust/1/files" name="Trust Files" />
            <PageLink to="/trust/1/users" name="Trust Users" />
            <Login className="ml-auto h-fit-content self-center" />
        </nav>
    );
}
