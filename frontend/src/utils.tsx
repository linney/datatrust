import { Datatrust } from "./openapi";

export const ExampleDatatrust: Datatrust = {
    id: "0",
    name: "Example Datatrust",
    description: "Example Datatrust Description",
    source: "University of Manitoba",
    access: [
        {
            id: "0",
            name: "John Doe",
        },
    ],
    stakeholders: [],
};

export function checkDatasetAccess(
    dataset: Datatrust,
    user_id: string
): boolean {
    return dataset.access.some((access) => access.id === user_id);
}
