import React from "react";
import { Route, BrowserRouter, Routes } from "react-router-dom";
import Web3 from "web3";
import Footer from "./components/Footer";
import Navbar from "./components/Navbar";
import { Datatrust } from "./openapi";
import CreateDatatrust from "./Pages/CreateDatatrust";
import CreateUser from "./Pages/CreateUser";
import DatatrustFiles from "./Pages/DatatrustFiles";
import DatatrustList from "./Pages/DatatrustList";
import DatatrustRequests from "./Pages/DatatrustRequests";
import { User, UserContext } from "./utils/User";
import { Web3Context } from "./utils/Web3";
import { ExampleDatatrust } from "./utils";
import DatatrustUsers from "./Pages/DatatrustUsers";
import DatatrustInfo from "./Pages/DatatrustInfo";

function App() {
    const [user, setUser] = React.useState<User | null>(null);
    const [web3, setWeb3] = React.useState<Web3 | null>(null);
    const exampleTrust: Datatrust = ExampleDatatrust;

    return (
        <UserContext.Provider value={{ user, setUser }}>
            <Web3Context.Provider value={{ web3, setWeb3 }}>
                <BrowserRouter>
                    <Navbar />
                    <Routes>
                        <Route path="/" element={<DatatrustList />} />
                        <Route
                            path="/requests"
                            element={<DatatrustRequests />}
                        />
                        <Route path="/user" element={<CreateUser />} />
                        <Route path="/trust">
                            <Route path=":id" element={<DatatrustInfo />} />
                            <Route
                                path=":id/files"
                                element={
                                    <DatatrustFiles datatrust={exampleTrust} />
                                }
                            />
                            <Route
                                path=":id/users"
                                element={
                                    <DatatrustUsers trust={exampleTrust} />
                                }
                            />

                            <Route
                                path="create"
                                element={<CreateDatatrust />}
                            />
                        </Route>
                    </Routes>
                    <Footer />
                </BrowserRouter>
            </Web3Context.Provider>
        </UserContext.Provider>
    );
}

export default App;
