import { useEffect, useState } from "react";
import { DatatrustBreadcrumbs } from "../components/Breadcrumbs";
import { Datatrust } from "../openapi";

async function getDatatrustUsers(trust: Datatrust) {
    const users = [
        "0x0000000000000000000000000000000000000000",
        "0x0000000000000000000000000000000000000001",
    ];

    return users;
}

function UserRemovalButton(user: string) {
    return (
        <>
            <button
                type="button"
                className="justify-self-end inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 ml-4 font-medium rounded-md shadow-sm text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
                Remove
            </button>
        </>
    );
}

export default function DatatrustUsers(props: { trust: Datatrust }) {
    const [users, setUsers] = useState<string[]>([]);

    useEffect(() => {
        getDatatrustUsers(props.trust).then(setUsers);
    }, [props.trust]);

    return (
        <>
            <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <div className="max-w-3xl mx-auto">
                    <h1 className="pt-8 text-2xl font-bold leading-7 text-gray-900 sm:text-3xl sm:truncate">
                        Datatrust User Management
                    </h1>
                    <DatatrustBreadcrumbs
                        datatrust={props.trust}
                        page={{
                            name: "Users",
                            path: `/trust/${props.trust.id}/users`,
                        }}
                    />
                    <div className="bg-white shadow rounded-md max-w-7xl mx-auto ">
                        <ul role="list" className="divide-y divide-gray-200">
                            {users.map((user) => (
                                <li
                                    key={user}
                                    className="px-6 py-4 text-gray-700 font-mono grid grid-cols-2"
                                >
                                    {user}
                                    {UserRemovalButton(user)}
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            </div>
        </>
    );
}
