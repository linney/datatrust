import { useCallback, useEffect, useState } from "react";
import { Datatrust, DatatrustApi } from "../openapi";
import { XCircleIcon } from "@heroicons/react/solid";
import {
    UploadIcon,
    CloudUploadIcon,
    ChevronRightIcon,
} from "@heroicons/react/outline";

import { useDropzone } from "react-dropzone";
import { DatatrustBreadcrumbs } from "../components/Breadcrumbs";

export default function DatatrustFiles(props: { datatrust: Datatrust }) {
    const [files, setFiles] = useState<string[]>([]);
    const onDrop = useCallback((acceptedFiles) => {
        setFiles(acceptedFiles);
        console.log(acceptedFiles);
    }, []);

    const { getRootProps, getInputProps, isDragActive } = useDropzone({
        onDrop,
    });

    const { datatrust } = props;

    useEffect(() => {
        const api = new DatatrustApi();
        api.getDatatrustContentTrustsTrustIdContentGet({
            trustId: datatrust.id,
        }).then((_files) => setFiles(_files));
    }, [datatrust.id]);

    return (
        <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            <div className="max-w-3xl mx-auto">
                <h1 className="pt-8 text-2xl font-bold leading-7 text-gray-900 sm:text-3xl sm:truncate">
                    Datatrust File Management
                </h1>
                <DatatrustBreadcrumbs
                    datatrust={datatrust}
                    page={{
                        name: "Files",
                        path: `/trust/${datatrust.id}/files`,
                    }}
                />
                <div {...getRootProps()}>
                    <div className="relative block w-full border-2 border-gray-300 border-dashed rounded-lg p-8 text-center hover:border-gray-400 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        <input {...getInputProps()} />
                        {isDragActive ? (
                            <>
                                <CloudUploadIcon className="mx-auto h-12 text-gray-500" />
                                <span className="mt-2 block text-sm font-medium text-gray-900">
                                    Release to upload
                                </span>
                            </>
                        ) : (
                            <>
                                <UploadIcon className="mx-auto h-12 text-gray-500" />
                                <span className="mt-2 block text-sm font-medium text-gray-900">
                                    Upload a new file to the trust.
                                </span>
                            </>
                        )}
                    </div>
                </div>
                <div className="bg-white shadow overflow-hidden sm:rounded-md mt-4">
                    {files.length > 0 ? (
                        <ul className="divide-y divide-gray-200">
                            {files.map((file) => (
                                <li
                                    key={file}
                                    className="px-4 py-4 sm:px-6 flex justify-between"
                                >
                                    <p className="font-mono">{file}</p>
                                    <button className="flex">
                                        <XCircleIcon className="h-6 text-red-600 hover:text-red-700" />
                                    </button>
                                </li>
                            ))}
                        </ul>
                    ) : (
                        <h2 className="p-4">No files in datatrust.</h2>
                    )}
                </div>
            </div>
        </div>
    );
}
