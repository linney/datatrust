import { Datatrust } from "../openapi";
import { ExampleDatatrust } from "../utils";

type DatatrustRequestType = {
    datatrust: Datatrust;
    requester: {
        address: string;
        name: string;
    };
    description: string;
};

function DatatrustRequest(props: { request: DatatrustRequestType }) {
    const { request } = props;

    return (
        <div className="grid grid-cols-2 shadow mx-12 p-8 max-w-4xl bg-white">
            <div>
                <h1 className="text-xl font-bold">{request.requester.name}</h1>
                <code>{request.requester.address}</code>
            </div>
            <div className="justify-self-end">
                <h1 className="text-xl font-bold">{request.datatrust.name}</h1>
                <h2>{request.datatrust.source}</h2>
            </div>
            <p className="col-span-2">{request.description}</p>
            <button className="mt-4 border-green-600 border-2 text-green-600 filter hover:shadow hover:drop-shadow font-bold py-2 px-4 rounded-full mx-1 justify-self-center">
                Grant Access
            </button>
            <button className="mt-4 border-red-500 border-2 text-red-500 filter hover:shadow hover:drop-shadow font-bold py-2 px-4 rounded-full mx-1 justify-self-center">
                Deny Access
            </button>
        </div>
    );
}

export default function DatatrustRequests() {
    const request: DatatrustRequestType = {
        datatrust: ExampleDatatrust,
        requester: {
            name: "John Doe",
            address: "0x1234567890123456789012345678901234567890",
        },
        description: "I would like to request access to this datatrust.",
    };

    return (
        <div className="grid justify-items-center">
            <h1 className="text-2xl font-bold text-center my-4">
                Datatrust Requests
            </h1>
            <DatatrustRequest request={request} />
        </div>
    );
}
