import { useEffect, useState } from "react";
import Modal from "../components/Modal";
import {
    Datatrust,
    Datatrust as DatatrustInfo,
    DatatrustApi,
} from "../openapi";
import { checkDatasetAccess, ExampleDatatrust } from "../utils";
import { DownloadIcon } from "@heroicons/react/outline";

function WithdrawButton(props: { datatrust: DatatrustInfo }) {
    return (
        <button className="mt-4 hover:shadow-md border-2 border-red-600 border-dashed text-red-600 font-bold py-2 px-4 rounded-full mx-2">
            Withdraw
        </button>
    );
}

function RequestAccessButton(props: { datatrust: DatatrustInfo }) {
    return (
        <button className="mt-4 mx-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full ">
            Request Access
        </button>
    );
}

function DataTrustCard(props: { datatrust: DatatrustInfo }) {
    const { datatrust } = props;

    return (
        <div className="rounded-md m-4 p-4 shadow justify-self-center h-fit-content sm:min-w-full lg:min-w-min lg:w-96 bg-white">
            <h1 className="text-xl font-bold">{datatrust.name}</h1>
            <h2 className="text-base text-black text-opacity-80 pb-2">
                {datatrust.source}
            </h2>
            <p>{datatrust.description}</p>
            {checkDatasetAccess(datatrust, "0") ? (
                <>
                    <WithdrawButton datatrust={datatrust} />
                    <DatatrustFilesModal datatrust={datatrust} />
                </>
            ) : (
                <RequestAccessButton datatrust={datatrust} />
            )}
        </div>
    );
}

function DatatrustFileDownloadButton(props: {
    datatrust: DatatrustInfo;
    file: string;
}) {
    return (
        <button
            type="button"
            className="inline-flex items-center px-2.5 py-1.5 border border-transparent text-xs font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            onClick={() => {
                const api = new DatatrustApi();

                api.downloadDatatrustTrustsTrustIdDownloadFilenameGet({
                    filename: props.file,
                    trustId: props.datatrust.id,
                }).then((response) => {
                    const url = window.URL.createObjectURL(
                        new Blob([response])
                    );
                    const link = document.createElement("a");
                    link.href = url;
                    link.setAttribute("download", props.file);
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                });
            }}
        >
            Download
            <DownloadIcon className="ml-2 -mr-1 h-5 w-5" aria-hidden="true" />
        </button>
    );
}

export function DatatrustFilesModal(props: { datatrust: DatatrustInfo }) {
    const { datatrust } = props;
    const [modalOpen, setModalOpen] = useState(false);

    const [files, setFiles] = useState<Array<string>>([]);

    useEffect(() => {
        const api = new DatatrustApi();

        api.getDatatrustContentTrustsTrustIdContentGet({
            trustId: datatrust.id,
        }).then((files) => {
            setFiles(files);
        });
    }, [datatrust.id]);

    return (
        <>
            <button
                onClick={() => {
                    setModalOpen(true);
                }}
                className="mt-4 mx-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
            >
                View Files
            </button>
            <Modal
                title={datatrust.name}
                open={modalOpen}
                setOpen={setModalOpen}
            >
                <ul className="divide-y divide-gray-200 w-full">
                    {files.map((file) => {
                        return (
                            <li
                                key={file}
                                className="py-4 flex items-center justify-between"
                            >
                                <div className="ml-3">
                                    <p className="text-sm font-medium text-gray-900">
                                        {file}
                                    </p>
                                </div>
                                <div className="ml-5 flex-shrink-0 text-sm">
                                    <DatatrustFileDownloadButton
                                        datatrust={datatrust}
                                        file={file}
                                    />
                                </div>
                            </li>
                        );
                    })}
                </ul>
            </Modal>
        </>
    );
}

export default function DatatrustList() {
    const no_access_trust: DatatrustInfo = {
        ...ExampleDatatrust,
        access: [{ id: "1", name: "Alice" }],
    };

    const [datatrusts, setDatatrusts] = useState<Datatrust[]>([]);

    useEffect(() => {
        const api = new DatatrustApi();
        api.getDatatrustsTrustsGet().then((datatrusts) => {
            setDatatrusts(datatrusts);
        });
    }, []);

    return (
        <div className="grid md:grid-cols-2 sm:grid-cols-1 gap-8 justify-center">
            <div>
                <h1 className="text-2xl font-bold text-center mt-4">
                    Accessible Datatrusts
                </h1>
                <div className="grid justify-items-center grid-cols-1 min-h-0">
                    {datatrusts.map((datatrust) => (
                        <DataTrustCard datatrust={datatrust} />
                    ))}
                </div>
            </div>

            <div>
                <h1 className="text-2xl font-bold text-center mt-4">
                    Request Access to Datatrusts
                </h1>
                <div className="grid justify-items-center grid-cols-1 min-h-0">
                    <DataTrustCard datatrust={no_access_trust} />
                    <DataTrustCard datatrust={no_access_trust} />
                </div>
            </div>
        </div>
    );
}
