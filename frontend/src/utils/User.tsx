import { createContext } from "react";

export type User = {
    username?: string;
    address: string;
};

type UserContextType = {
    user: User | null;
    setUser: (user: User) => void;
};

// @ts-ignore - Dumb, but this should never be undefined. If it is, it's a bug. But we should probably find a way to do this properly.
export const UserContext = createContext<UserContextType>(undefined);
