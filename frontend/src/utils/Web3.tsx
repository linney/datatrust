import { createContext } from "react";
import Web3 from "web3";

export type Web3ContextType = {
    web3: Web3 | null;
    setWeb3: (web3: Web3) => void;
};

export const Web3Context = createContext<Web3ContextType>({
    web3: null,
    setWeb3: () => {},
});
