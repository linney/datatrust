from typing import List, Union
import secrets

from decouple import config
from fastapi import APIRouter, Request, HTTPException, Form
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from jose import jwt

from datatrust.ethereum import get_signer


JWT_SECRET_KEY = config("SECRET_KEY")
NONCES: List[int] = []

router = APIRouter(prefix="/auth", tags=["auth"])


def verify_jwt(token: str) -> bool:
    try:
        jwt.decode(token, key=JWT_SECRET_KEY, algorithms=["HS256"])
        return True
    except:
        return False


def get_user_from_jwt(token: str) -> Union[str, None]:
    payload = jwt.decode(token, key=JWT_SECRET_KEY, algorithms=["HS256"])
    return payload.get("sub", None)


class JWTBearer(HTTPBearer):
    def __init__(self, auto_error: bool = True):
        super(JWTBearer, self).__init__(auto_error=auto_error)

    async def __call__(self, request: Request):
        credentials: HTTPAuthorizationCredentials = await super(
            JWTBearer, self
        ).__call__(request)
        if credentials:
            if not credentials.scheme == "Bearer":
                raise HTTPException(
                    status_code=403, detail="Invalid authentication scheme."
                )
            if not verify_jwt(credentials.credentials):
                raise HTTPException(
                    status_code=403, detail="Invalid token or expired token."
                )
            return get_user_from_jwt(credentials.credentials)
        else:
            raise HTTPException(status_code=403, detail="Invalid authorization code.")


@router.get("/start", response_model=str)
def start_auth() -> str:
    secret = secrets.randbelow(2 ** 32)
    NONCES.append(secret)

    return str(secret)


@router.post("/verify", response_model=str)
def verify_auth(nonce: int = Form(...), signed_nonce: str = Form(...)) -> str:
    if nonce not in NONCES:
        raise HTTPException(status_code=403, detail="Invalid nonce.")

    # Verify nonce, get addr
    addr = get_signer(str(nonce), signed_nonce)

    token = jwt.encode({"sub": addr, "nonce": nonce}, JWT_SECRET_KEY, algorithm="HS256")

    return token
