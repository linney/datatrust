import asyncio
import time

from fastapi import FastAPI
from fastapi_utils.tasks import repeat_every

from datatrust import trusts, ethereum, auth

from datatrust.models.datatrust import Datatrust

app = FastAPI(title="Datatrust API")

app.include_router(ethereum.api)
app.include_router(auth.router)
app.include_router(trusts.router)


@app.on_event("startup")
@repeat_every(seconds=15)
async def periodic_trust_tests():
    Datatrust.from_address.cache_clear()
    _trusts = Datatrust.list()

    start_time = time.time_ns()
    for trust in _trusts:
        trust.try_close_elections()
        await asyncio.sleep(0)

    end_time = time.time_ns()
    Datatrust.from_address.cache_clear()
    Datatrust.list()  # Refresh cache immediately
    print(f"Periodic trust tests took {((end_time - start_time)/1_000_000):.0f}ms")


def run():
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8000)


if __name__ == "__main__":
    run()
