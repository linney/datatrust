import json
from typing import List

from fastapi import APIRouter
from web3 import Web3
from decouple import config

from eth_account.messages import encode_defunct

api = APIRouter(tags=["testing"], prefix="/eth")

print(config("ETH_PROVIDER"))
w3 = Web3(Web3.HTTPProvider(config("ETH_PROVIDER")))
assert w3.isConnected()


@api.get("/accounts", response_model=List[str])
def accounts():
    return w3.eth.accounts


@api.get("/balance/{address}", response_model=int)
def balance(address: str):
    return w3.eth.getBalance(address)


@api.get("/block/latest", response_model=int)
def block_latest():
    return w3.eth.blockNumber


@api.get("/trust_manager/address", response_model=str)
def manager_address():
    from datatrust.models.datatrust import CONTRACT_ADDRESS

    return CONTRACT_ADDRESS


@api.get("/trust_manager/abi", response_model=List[dict])
def manager_abi():
    from datatrust.models.datatrust import DATATRUST_MANAGER_ABI

    return json.loads(DATATRUST_MANAGER_ABI)


@api.get("/trust/abi", response_model=list)
def trust_abi():
    from datatrust.models.datatrust import DATATRUST_ABI

    return json.loads(DATATRUST_ABI)


def get_signer(unsigned: str, signed: str) -> str:
    message = encode_defunct(text=unsigned)

    return w3.eth.account.recover_message(message, signature=signed)
