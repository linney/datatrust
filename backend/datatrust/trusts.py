from fastapi import APIRouter, Depends, HTTPException, UploadFile, File

from datatrust.auth import JWTBearer
from datatrust.models.datatrust import Datatrust

import os
import uuid
from typing import List
import decouple

from fastapi.responses import FileResponse

DATA_DIR = decouple.config("DATA_DIR")

router = APIRouter(prefix="/trusts", tags=["Trusts"])


def clear_trust_cache():
    Datatrust.from_address.clear_cache()


@router.get("/", response_model=List[Datatrust])
async def get_datatrusts() -> List[Datatrust]:
    return Datatrust.list()


@router.get("/{trust_id}", response_model=Datatrust)
async def get_datatrust(trust_id: str) -> Datatrust:
    trusts = Datatrust.list()

    trusts = [trust for trust in trusts if trust.id == trust_id]
    if not trusts:
        raise HTTPException(status_code=404, detail="Trust not found")

    return trusts[0]


@router.get("/{trust_id}/content", response_model=List[str])
async def get_datatrust_content(
    trust_id: str, user: str = Depends(JWTBearer())
) -> List[str]:
    trust = Datatrust.from_address(trust_id)
    if not trust:
        raise HTTPException(status_code=404, detail="Trust not found")

    if user not in trust.users and user not in trust.stakeholders:
        raise HTTPException(status_code=403, detail="User not authorized")

    try:
        return os.listdir(f"{DATA_DIR}/{trust_id}")
    except FileNotFoundError as e:
        return []


@router.post("/{trust_id}/content", response_model=str)
async def upload_datatrust_content(
    trust_id: str,
    file: UploadFile = File(...),
    user: str = Depends(JWTBearer()),
) -> str:
    trust = Datatrust.from_address(trust_id)
    if not trust:
        raise HTTPException(status_code=404, detail="Trust not found")

    if user not in trust.stakeholders:
        raise HTTPException(status_code=403, detail="User not authorized")

    filename = file.filename
    trust_path = f"{DATA_DIR}/{trust_id}"
    file_path = f"{trust_path}/{filename}"

    if not os.path.exists(trust_path):
        os.makedirs(trust_path, exist_ok=True)

    if os.path.exists(file_path):
        raise HTTPException(status_code=409, detail="File already exists")

    with open(file_path, "wb") as f:
        f.write(file.file.read())

    return filename


@router.get(
    "/{trust_id}/download/{filename}",
    response_class=FileResponse,
    responses={
        200: {
            "content": {"application/octet-stream": {"schema": {"type": "file"}}},
            "description": "Returns the file.",
        }
    },
    tags=["datatrust"],
)
async def download_datatrust(
    trust_id: str, filename: str, user: str = Depends(JWTBearer())
):
    trust = Datatrust.from_address(trust_id)
    if not trust:
        raise HTTPException(status_code=404, detail="Trust not found")

    if user not in trust.users and user not in trust.stakeholders:
        raise HTTPException(status_code=403, detail="User not authorized")

    file_path = f"{DATA_DIR}/{trust_id}/{filename}"
    if not os.path.exists(file_path):
        raise HTTPException(status_code=404, detail="File not found")

    return FileResponse(
        path=file_path,
        filename=filename,
        media_type="application/octet-stream",
    )
