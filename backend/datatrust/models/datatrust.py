from functools import lru_cache

from pydantic import BaseModel
from typing import List

from web3.contract import Contract

from datatrust.ethereum import w3
from decouple import config

CONTRACT_ADDRESS = config("CONTRACT_ADDRESS")
DATATRUST_MANAGER_ABI = '[{"inputs":[],"stateMutability":"nonpayable","type":"constructor"},{"inputs":[{"internalType":"string","name":"_name","type":"string"},{"internalType":"string","name":"_description","type":"string"},{"internalType":"string","name":"_source","type":"string"}],"name":"addDatatrust","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"datatrusts","outputs":[{"internalType":"contract Datatrust","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getDatatrusts","outputs":[{"internalType":"contract Datatrust[]","name":"","type":"address[]"}],"stateMutability":"view","type":"function"}]'
DATATRUST_ABI = '[{"inputs":[{"internalType":"string","name":"_name","type":"string"},{"internalType":"string","name":"_description","type":"string"},{"internalType":"string","name":"_source","type":"string"},{"internalType":"address","name":"_owner","type":"address"}],"stateMutability":"nonpayable","type":"constructor"},{"inputs":[{"internalType":"address","name":"_user","type":"address"}],"name":"CheckIfUserIsUser","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"candidateUsers","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"candidates","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"candidate","type":"address"}],"name":"checkIfAllVotedElection","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"candidate","type":"address"}],"name":"checkIfAllVotedRemoval","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"candidate","type":"address"}],"name":"checkIfAllVotedUserElection","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"user","type":"address"}],"name":"checkIfUserIsStakeholder","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"candidate","type":"address"}],"name":"closeElection","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"candidate","type":"address"}],"name":"closeRemoval","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"candidate","type":"address"}],"name":"closeUserElection","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"description","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"elections","outputs":[{"internalType":"address","name":"candidate","type":"address"},{"internalType":"uint256","name":"start","type":"uint256"},{"internalType":"bool","name":"nayVote","type":"bool"},{"internalType":"bool","name":"active","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getCandidateUsers","outputs":[{"internalType":"address[]","name":"","type":"address[]"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getCandidates","outputs":[{"internalType":"address[]","name":"","type":"address[]"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getRemovalCandidates","outputs":[{"internalType":"address[]","name":"","type":"address[]"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getStakeholders","outputs":[{"internalType":"address[]","name":"","type":"address[]"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getUsers","outputs":[{"internalType":"address[]","name":"","type":"address[]"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"candidate","type":"address"}],"name":"proposeCandidate","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"candidate","type":"address"}],"name":"proposeRemoval","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_user","type":"address"}],"name":"proposeUser","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"removals","outputs":[{"internalType":"address","name":"candidate","type":"address"},{"internalType":"uint256","name":"start","type":"uint256"},{"internalType":"bool","name":"nayVote","type":"bool"},{"internalType":"bool","name":"active","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"removalsCandidates","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_user","type":"address"}],"name":"removeUser","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"source","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"stakeHolders","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"userElections","outputs":[{"internalType":"address","name":"candidate","type":"address"},{"internalType":"uint256","name":"start","type":"uint256"},{"internalType":"bool","name":"nayVote","type":"bool"},{"internalType":"bool","name":"active","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"users","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"candidate","type":"address"},{"internalType":"bool","name":"_vote","type":"bool"}],"name":"vote","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"candidate","type":"address"},{"internalType":"bool","name":"_vote","type":"bool"}],"name":"voteRemove","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"candidate","type":"address"},{"internalType":"bool","name":"_vote","type":"bool"}],"name":"voteUser","outputs":[],"stateMutability":"nonpayable","type":"function"}]'

manager = w3.eth.contract(address=CONTRACT_ADDRESS, abi=DATATRUST_MANAGER_ABI)


class Datatrust(BaseModel):
    id: str
    name: str
    description: str
    source: str  # Ex. University of Manitoba
    users: List[str]
    stakeholders: List[str]

    @property
    def contract(self) -> Contract:
        return w3.eth.contract(address=self.id, abi=DATATRUST_ABI)

    @staticmethod
    def create(name: str, description: str, source: str) -> "Datatrust":
        """
        Create a new datatrust.
        """
        tx_id = manager.functions.addDatatrust(name, description, source).transact()
        w3.eth.wait_for_transaction_receipt(tx_id)
        Datatrust.from_address.cache_clear()
        trusts = Datatrust.list()

        return [trust for trust in trusts if trust.name == name][0]

    @staticmethod
    def list() -> List["Datatrust"]:
        d_addrs = manager.functions.getDatatrusts().call()
        return [Datatrust.from_address(addr) for addr in d_addrs]

    @staticmethod
    @lru_cache
    def from_address(address: str) -> "Datatrust":
        trust_contract = w3.eth.contract(address=address, abi=DATATRUST_ABI)

        return Datatrust(
            id=address,
            name=trust_contract.functions.name().call(),
            description=trust_contract.functions.description().call(),
            source=trust_contract.functions.source().call(),
            users=trust_contract.functions.getUsers().call(),
            stakeholders=trust_contract.functions.getStakeholders().call(),
        )

    def try_close_elections(self):
        candidates = self.contract.functions.getCandidates().call()
        for candidate in candidates:
            try:
                self.contract.functions.closeElection(candidate).transact()
            except Exception as e:
                continue

        user_candidates = self.contract.functions.getCandidateUsers().call()
        for candidate in user_candidates:
            try:
                self.contract.functions.closeUserElection(candidate).transact()
            except Exception as e:
                continue

        removal_candidates = self.contract.functions.getRemovalCandidates().call()
        for candidate in removal_candidates:
            try:
                self.contract.functions.closeRemoval(candidate).transact()
            except Exception as e:
                continue

    def propose_user(self, user: str):
        self.contract.functions.proposeUser(user).transact()

    def propose_stakeholder(self, stakeholder: str):
        self.contract.functions.proposeCandidate(stakeholder).transact()

    def propose_removal(self, stakeholder: str):
        self.contract.functions.proposeRemoval(stakeholder).transact()

    def vote_stakeholder(self, user: str, vote: bool):
        self.contract.functions.vote(user, vote).transact()

    def vote_user(self, user: str, vote: bool):
        self.contract.functions.voteUser(user, vote).transact()

    def vote_removal(self, user: str, vote: bool):
        self.contract.functions.voteRemove(user, vote).transact()
