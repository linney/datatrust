# How to run and test
1. Run `poetry install` (to intall dependencies)
2. Run `poetry run start` (this will start a test web server. Note: This requires the hardhat network to be running. See the README.md one directory up.)
3. Run `poetry run pytest` (this will run the tests. Note: this will only work if the webserver is currently running)