import asyncio
import uuid
from contextlib import contextmanager
from tempfile import NamedTemporaryFile
import os
import filecmp

import pytest
from fastapi import FastAPI
from decouple import config

from starlette.testclient import TestClient
from web3 import Web3

from datatrust.app import app
from datatrust.models.datatrust import Datatrust

w3 = Web3(Web3.HTTPProvider(config("ETH_PROVIDER")))


@contextmanager
def AuthorisedClient(_app: FastAPI) -> TestClient:
    client = TestClient(_app)
    res = client.get("/auth/start")
    res.raise_for_status()
    nonce = res.json()

    signed_nonce = w3.eth.sign(w3.eth.accounts[0], text=nonce)
    res = client.post(
        "/auth/verify", data={"nonce": nonce, "signed_nonce": signed_nonce.hex()}
    )
    res.raise_for_status()

    jwt = res.json()
    client.headers["Authorization"] = f"Bearer {jwt}"
    yield client


def test_auth_flow():
    with AuthorisedClient(app) as client:
        res = client.get("/trusts/")
        res.raise_for_status()

        trusts = res.json()
        assert len(trusts) >= 1

        res = client.get(f"/trusts/{trusts[0]['id']}/content")
        res.raise_for_status()


def test_upload_download():
    with AuthorisedClient(app) as client:
        with NamedTemporaryFile() as f:
            f.write(os.urandom(1024 ** 2))  # 1MB
            f.seek(0)

            res = client.get("/trusts/")
            res.raise_for_status()

            trust = Datatrust(**res.json()[0])

            res = client.post(
                f"/trusts/{trust.id}/content",
                files={"file": (uuid.uuid4().hex, f)},  # Specify a random filename
            )
            res.raise_for_status()

            filename = res.json()

            res = client.get(f"/trusts/{trust.id}/download/{filename}")
            res.raise_for_status()

            with NamedTemporaryFile() as f2:
                for chunk in res.iter_content(chunk_size=1024):
                    f2.write(chunk)
                f2.seek(0)

                assert filecmp.cmp(f.name, f2.name)


@pytest.mark.asyncio
async def test_background_user_vote_proposal():
    with AuthorisedClient(app) as client:

        def get_trust_with_name(_name: str) -> Datatrust:
            Datatrust.from_address.cache_clear()  # Explicitly clear cache
            trusts = client.get("/trusts/").json()
            for trust in trusts:
                if trust["name"] == _name:
                    return Datatrust(**trust)

        name = uuid.uuid4().hex
        trust = Datatrust.create(
            name=name,
            description="A trust used for testing user proposals.",
            source="Pytest",
        )
        user_b = w3.eth.accounts[1]

        assert user_b not in trust.users
        trust.propose_user(user_b)
        trust.vote_user(user_b, True)

        sleep_total = 30
        sleep_steps = 15
        sleep_step = sleep_total / sleep_steps
        for _ in range(sleep_steps):
            await asyncio.sleep(sleep_step)

            new_trust = get_trust_with_name(name)
            if user_b in new_trust.users:
                break
        else:
            raise Exception("User not added to trust")


@pytest.mark.asyncio
async def test_background_stakeholder_vote_proposal():
    with AuthorisedClient(app) as client:

        def get_trust_with_name(_name: str) -> Datatrust:
            Datatrust.from_address.cache_clear()  # Explicitly clear cache
            trusts = client.get("/trusts/").json()
            for trust in trusts:
                if trust["name"] == _name:
                    return Datatrust(**trust)

        name = uuid.uuid4().hex
        trust = Datatrust.create(
            name=name,
            description="A trust used for testing stakeholder proposals.",
            source="Pytest",
        )
        user_b = w3.eth.accounts[1]

        assert user_b not in trust.users
        trust.propose_stakeholder(user_b)
        trust.vote_stakeholder(user_b, True)

        sleep_total = 30
        sleep_steps = 15
        sleep_step = sleep_total / sleep_steps
        for _ in range(sleep_steps):
            await asyncio.sleep(sleep_step)

            new_trust = get_trust_with_name(name)
            if user_b in new_trust.stakeholders:
                break
        else:
            raise Exception("User not added to trust")


@pytest.mark.asyncio
async def test_background_stakeholder_removal_proposal():
    with AuthorisedClient(app) as client:

        def get_trust_with_name(_name: str) -> Datatrust:
            Datatrust.from_address.cache_clear()  # Explicitly clear cache
            trusts = client.get("/trusts/").json()
            for trust in trusts:
                if trust["name"] == _name:
                    return Datatrust(**trust)

        name = uuid.uuid4().hex
        trust = Datatrust.create(
            name=name,
            description="A trust used for testing stakeholder proposals.",
            source="Pytest",
        )
        user_b = w3.eth.accounts[1]

        assert user_b not in trust.users
        trust.propose_stakeholder(user_b)
        trust.vote_stakeholder(user_b, True)

        sleep_total = 30
        sleep_steps = 15
        sleep_step = sleep_total / sleep_steps
        for _ in range(sleep_steps):
            await asyncio.sleep(sleep_step)

            new_trust = get_trust_with_name(name)
            if user_b in new_trust.stakeholders:
                break
        else:
            raise Exception("User not added to trust")

        trust.propose_removal(user_b)
        trust.vote_removal(user_b, True)

        sleep_total = 30
        sleep_steps = 15
        sleep_step = sleep_total / sleep_steps
        for _ in range(sleep_steps):
            await asyncio.sleep(sleep_step)

            new_trust = get_trust_with_name(name)
            if user_b not in new_trust.stakeholders:
                break
        else:
            raise Exception("User not added to trust")
