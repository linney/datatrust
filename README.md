# Datatrust

## Current State
The ethereum smart contract and local devolpment chain (via Hardhat) as well as the off-chain datatrust backend (handling file upload/download, and chain polling) are 100% functional.

The frontend is incomplete. It interacts with the backend to get some information, and connects to metamask for interacting with the chain directly. However the full interface is not complete. There a features missing and many features are just mocked.

The frontend is the least important (from a proof concept perspective), thus development was halted to polish the rest.

## Building/Usage/Testing
The frontend building requires extra work compared to the smart contract and backend, and is not included in this section.

There are 2 suites of tests (that cover the full functionality of the trust). The first suite is entirely dedicated to the smart contract, and tests everything other than the file upload/download and polling of the smart contract. The second suite tests the backend interface, ensuring that polling and and the file upload/download is functional, and access controlled.

Building and running of the ethereum chain/smart contract and the backend are abstracted away from language specific details by employing two technologies that must be installed on your system to test this. The first is [Just](https://github.com/casey/just#installation) which is a Make like command runner without some of the clunkiness and issues with Make. The second of which is [Docker](https://docs.docker.com/engine/install/) which provides isolated environments to make the build process (hopefully) completely cross-platform. That being said I only have machines running linux, so if you do encounter any issues building or running, please reach out to me.

With these two tools installed deploying and testing this project is extremely simple.

### Running and Testing Ethereum/Smart Contracts
To test, simply
1. `cd ./ethereum`
2. `just test`

And the project will be compiled and the tests will be ran.

To run an ethereum node (so the backend can interact with it), simply
1. `cd ./ethereum`
2. `just run`

This will compile the project and deploy an ethereum node with a smart contract manager deployed at `0x5FbDB2315678afecb367f032d93F642f64180aa3`.

NOTE: Exiting via `CTRL-c` won't stop the docker image. To stop the running docker image, run `just stop`

### Running and Testing the Backend
To test, first start up an ethereum node (see above section) then simply,
1. `cd ./backend`
2. `just test` (this will take a minute or so as it verifies the background polling happens, which only occurs every 10s by default [Note: on a real chain, this interval would be much longer])

To run the webserver (so you can interact with it directly via http) simply,
1. `cd ./backend`
2. `just run`

This will deploy the backend and expose it on port 8000. The backend uses fastapi, so you can view all the routes by visiting http://localhost:8000/docs .

NOTE: Exiting via `CTRL-c` won't stop the docker image. To stop the running docker image, run `just stop`

### Pre-recorded Execution
See [![asciicast](https://asciinema.org/a/h9D3Q2SEIEXNxsZjl8OopwL07.svg)](https://asciinema.org/a/h9D3Q2SEIEXNxsZjl8OopwL07) . for a pre-recorded screencast showing these tests being ran.
## Vision
The goal is an elementary datatrust, allowing stakeholders within a trust to grant access to users.

Stakeholders can be elected and removed (requiring absolute consensus). Users can then be granted access to a datatrust via having a stakeholder submit a proposal, which can then be accepted either via having all stakeholders vote yes, or having 24h pass with no one saying no. Users can then be removed via any stakeholder without consensus. The rationale for this is that users should only be allowed to process data under absolute consensus. This is likely not optimal for real world use cases, but that consideration is out of scope for this project.

Users can then browse, upload, and download data from the datatrust after authorising themselves. Downloading will be off-chain, but the browsing and ACL will be based on the current state of the chain.

Once the platform is built, I will do some simulations with bad actors to determine the weakpoints around data security. These bad actors will tested in the form of users, and stakeholders. I will then attempt to describe a few possible solutions to some of these weakpoints.
