docker run -d -p 0.0.0.0:80:80 \
    --name datatrust-proxy \
    -v $PWD/Caddyfile:/etc/caddy/Caddyfile \
    -e FRONTEND_URL="192.168.1.159:3000" \
    -e BACKEND_URL="192.168.1.159:8000" \
    caddy:2
