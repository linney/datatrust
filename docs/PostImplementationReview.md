# Post Implementation Review of my COMP-7570 Term Project
## Jacob Linney

1. What are the problem statement and objectives of you project?

The goal is an elementary datatrust, allowing stakeholders within a trust to grant access to users.

Stakeholders can be elected and removed (requiring absolute consensus). Users can then be granted access to a datatrust via having a stakeholder submit a proposal, which can then be accepted either via having all stakeholders vote yes, or having 24h pass with no one saying no. Users can then be removed via any stakeholder without consensus. The rationale for this is that users should only be allowed to process data under absolute consensus. This is likely not optimal for real world use cases, but that consideration is out of scope for this project.

Users can then browse, upload, and download data from the datatrust after authorising themselves. Downloading will be off-chain, but the access control logic will be based on the current state of the chain.

Once the platform is built, I will do some simulations with bad actors to determine the weakpoints around data security. These bad actors will tested in the form of users, and stakeholders. I will then attempt to describe a few possible solutions to some of these weakpoints.

2. What is the outcome of your project? Did you achieve your project goals?

Overall, I feel the project was successful. The platform was built without any major issues, and I was able to able to identify some key weakpoints and possible solutions to them.

3. What were your challenges in this project?

Interestingly, all of the major challenges I ran into were consequences of using Ethereum as the blockchain. The lack of identity management was the most major issue, and one I hadn't considered going into the project. Everything else largely went very smoothly. 

4. What resources did you use to learn the skills required to complete the project? What tools and technologies have you applied?

In terms of learning, I only really used the [Web3.js Docs](https://web3js.readthedocs.io/en/v1.5.2/web3.html), [Web3.py Docs](https://w.eb3py.readthedocs.io/en/stable/) for learning how to interact with Ethereum via Javascript and Python respectively. The rest of the tools and technologies I used were things I was already somewhat to very familiar with.

Project Management: [GitLab](https://gitlab.com)

Architecture Diagrams/Planning: [Excalidraw](https://excalidraw.com/)

Frontend: (Javascript)

- Static Typing: [TypeScript](https://www.typescriptlang.org/)
- Application Framework: [React.js](https://reactjs.org/)
- CSS Utils: [TailwindCSS](https://tailwindcss.com/)

Backend: (Python)

- Dependency Management: [Poetry](https://python-poetry.org/)
- Web API: [FastAPI](https://fastapi.tiangolo.com/)
- JWTs: [JOSE](https://pypi.org/project/python-jose/)

Blockchain: (Ethereum)

- Development Environment: [Hardhat](https://hardhat.org/)

5. What are your suggestions to improve the project for future development?

There are two primary routes of improvement I can see. The first of which is to lean on identity via [ERC-725](https://docs.ethhub.io/built-on-ethereum/identity/ERC725/), and address the few security issues with the some of the mitigations I suggest in my final paper. (Ex. timed-deletes of items).

The other route is to refactor the code to work on another chain, specifically [Hyperledger Fabric](https://www.hyperledger.org/use/fabric) and attempt to address the issues I had at the chain level using [Hyperledger Indy](https://www.hyperledger.org/use/hyperledger-indy) and perhaps utilise multi-signatures transaction in place of actions that otherwise required smart contract validation in Ethereum. From my (limited) research, it appears that [Fabric Endorsement Policies](https://hyperledger-fabric.readthedocs.io/en/latest/endorsement-policies.html) could be used for this functionality.

I think the second route is the better one, as it would allow faster transactions as well, which are pretty slow on Ethereum.

There is one last improvement, which is more about developer quality of life, and making it easier for people to try and deploy the project. I spent a little time considering how this could be deployed, but didn't end up packaging an easy solution. I usually try to have deployment as simple as `make deploy`, but this project was finalised with effectively having three distinct deployments that needed to be done. (The frontend, backend, and contract deployment)

6. What lessons have you learned from conducting this project? What would you have done differently?

My biggest lesson learned from this project was to spend more time considering what the ideal blockchain technology would support. Multi-signature transactions and Identity management were two things I didn't truly consider until my project was well underway. If I had spent more time considering the projects needs before settling on Ethereum, I might have ended up with a more suitable technology such as Hyperledger Fabric.
