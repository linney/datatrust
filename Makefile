SHELL := /bin/bash

generate_ts_models:
	docker run --rm \
		--name="dataset-ts-generator" \
		-v $$(readlink -f ./frontend):/frontend \
		openapitools/openapi-generator-cli \
		generate \
		-g typescript-fetch \
		-i http://hades.local/v1/openapi.json \
		-o /frontend/src/openapi \
		--additional-properties=supportsES6=true,modelPropertyNaming=original,typescriptThreePlus=true
